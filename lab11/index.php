<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Лабораторная 11</title>
    <link href="style.css" rel="stylesheet">
</head>
<body>
<div class="wrapper">
    <header class="head" id="head">
        <table>
            <tr><th colspan="2" height="20"><div>
                <div>
                    <span></span>
                </div> </th>
            </tr> 
            <tr><td><div>
                <div>
                   <img src="logo.jpg" width="300"> 
                </div></td>
                <td>
                    <p class="page-title"><h1>Толкачева Елизавета Викторовна 211-361</h1></p>
                    <p><h1>Лабораторная работа №11</h1></p>
                </td></tr> 
            </table>
    </header>
<?php
$type = "table";
$table = "all";
if (isset($_GET['table'])) {
    $table = $_GET['table'];
}

if (isset($_GET['type'])) {
    $type = $_GET['type'];
}
?>
<header>
    <nav>
        <ul>
            <li>
                <a <?php if ($type == 'table') {
                    echo 'class="selected"';
                }?> href="?type=table&table=all">Табличная вёрстка</a></li>
            <li>
                <a <?php if ($type == 'block') echo 'class="selected"'?> href="?type=block&table=all">Блочная вёрстка</a></li>
        </ul>
    </nav>
</header>
<main>
    <menu class="left">

        <li><a <?php if ($table == 'all') echo 'class="selected"'?> href="?table=all&type=<?php echo $type; ?>">Всё</a></li>
        <?php
            for ($i = 2; $i <= 10; $i++) {
                $selected = "";
                if ($table == $i)
                    $selected = 'class="selected"';
                echo '<li><a '.$selected.' href="?table='.$i.'&type='.$type.'">'.$i.'</a></li>';
            }

            function all_table($table, $type): void
            {
            echo '<table class="table">';
                for ($i = 1; $i <= 10; $i++) {
                    echo '<tr>';
                    echo '<td><a href="?table='.$i.'&type='.$type.'">' . $i . '</a></td>';
                    echo '<td><p><a href="?table='.$i.'&type='.$type.'">' . $i . '</a>*<a href="?table='.$table.'&type='.$type.'">' . $table . '</a>='.($table * $i <= 10 ? '<a href="?table='.$i*$table.'&type='.$type.'">' . $i*$table . '</a>': $table*$i).'</p></td>';
                    echo '</tr>';
                }
                echo '</table>';
        }
            function div_table($table, $type): void {
                echo '<div class="div-table">';
                for ($i = 1; $i <= 10; $i++) {
                    echo '<div class="row">';
                    echo '<div class="col"><a href="?table='.$i.'&type='.$type.'">' . $i . '</a></div>';
                    echo '<div class="col"><p><a href="?table='.$i.'&type='.$type.'">' . $i . '</a>*<a href="?table='.$table.'&type='.$type.'">' . $table . '</a>='.($table * $i <= 10 ? '<a href="?table='.$i*$table.'&type='.$type.'">' . $i*$table . '</a>': $table*$i).'</p></div>';
                    echo '</div>';
                }
                echo '</div>';
            }
        ?>
    </menu>
    <section>
        <?php
        function render_all($type): void
        {
            switch ($type) {
                case 'table':
                    echo '<table class="table">';
                    for ($i = 1; $i <= 10; $i++) {
                        echo '<tr>';
                        for ($j = 1; $j <= 10; $j++) {
                            echo '<td><p><a href="?table=' . $i . '&type=' . $type . '">' . $i . '</a>*<a href="?table=' . $j . '&type=' . $type . '">' . $j . '</a>=' . ($j * $i <= 10 ? '<a href="?table=' . $i * $j . '&type=' . $type . '">' . $i * $j . '</a>' : $j * $i) . '</p></td>';
                        }
                        echo '</tr>';
                    }
                    echo '</table>';
                    break;
                case 'block':
                 echo '<div class="div-table">';
                for ($i = 1; $i <= 10; $i++) {
                    echo '<div class="row">';
                    for ($j = 1; $j <= 10; $j++) {
                        echo '<div class="col"><p><a href="?table='.$i.'&type='.$type.'">' . $i . '</a>*<a href="?table='.$j.'&type='.$type.'">' . $j . '</a>='.($j * $i <= 10 ? '<a href="?table='.$i*$j.'&type='.$type.'">' . $i*$j . '</a>': $j*$i).'</p></div>';
                    }
                    echo '</div>';
                }
                echo '</div>';
                break;
            };
        }

        function render_specific($table, $type): void
        {
            switch($type) {
                case 'table':
                 all_table($table, $type);
                break;
                case 'block':
                div_table($table, $type);
                break;
            };
        }
        if ($table == 'all') {
            render_all($type);
        } else {
            render_specific($table, $type);
        }
        ?>
    </section>
</main>
<footer>
    <div><h2>Тип верстки </h2><?php echo $type ?></div>
    <div><h2>Тип верстки </h2><?php echo $table ?></div>
</footer>
</body>
</html>