<html lang="ru">
  <head>
    <meta charset="utf-8">
    <title><?php
        $title="Домашняя страница";
        echo $title;
    ?></title>
    <link rel="stylesheet" href="style.css">
  </head>
  <body>
    <header class="head" id="head">
      <p class="page-title">Блог</p>
      <nav class="blog-navigation">
      <?php $menu = [["index.php","Главная"],
      ["#skills","Мои навыки"],
      ["#achievments","Мои достижения"],
      ["feedback.php","Обратная связь"],
      ["login.php","Вход"],
      ["#foot","Мои контакты"]
    ];
    ?>

    <?php foreach($menu as $menu_item)
      { 
        $classname = "";
        if($menu_item[1]=="Главная") 
        $classname = "main";
      echo '<a class="menu '.$classname.'" href="'.$menu_item[0].'">'.$menu_item[1].'</a>';
      }
    ?>
      </nav>

    </header>
    <main>
      <article>
        <section class="mainpage" id="mainpage">
          <h2>Главная</h2>
          <p>Меня зовут Елизавета Толкачева, я начинающий IT-специалист. </p>
          <?php $s = date('s');	
            $os = $s % 2;	

            if( $os === 0 )	
            $name="https://sun9-14.userapi.com/impg/73jiHNGNxsBYacNCIAQlMll8F_8Kg4zA0Og6QQ/-J5Z9zRUojU.jpg?size=1104x1472&quality=96&sign=2404c58f0781d1fcb2a1e38d01270df1&type=album"; 
            else	
            $name="https://sun1-29.userapi.com/impg/v5tCUsQyi_UpJ8e3wWGRVejJHvWXn5JcYHZheA/2Qgt418lN3c.jpg?size=1620x2160&quality=95&sign=77a0a89592aa1966eaa16ff80b5499e9&type=album"; // сохраняем имя второго файла

            echo '<img src="'.$name.'" alt="Меняющаяся фотография" width="300">';
          ?>
          <p>Мне 18 лет, я учусь на 2 курсе Московского политехнического универистета по специальности "Прикладная информатика"</p>
        </section>
        <section class="skills" id="skills">
            <table class="table">
              <caption><h2>Мои навыки</h2></caption> 
              <tr>
                <td>
                  <div>
                    <h4>Java</h4>
                    <p>Опыт в написании клиент-серверных десктопных приложений</p>
                  </div>
                </td>
                <td>
                  <div>
                    <h4>Базы данных</h4>
                    <p>Опыт в создании баз данных, написании sql-запросов</p>
                  </div>
                </td>
                <td>
                  <div>
                    <h4>Тестирование</h4>
                    <p>Опыт в написании unit-тестов, ручном тестировании, составлении тест-кейсов</p>
                  </div>
                </td>
                <td>
                  <div>
                    <h4>Английский язык</h4>
                    <p>Уровень B1</p>
                  </div>
                </td>
              </tr>
          </table>
        </section>
        <section class="achievments" id="achievments">
          <h2>Мои достижения</h2>
          <p>Проектная деятельность в университете: База данных всех проектов</p>
          <p>Моя должность: разработчик базы данных</p>
          <p>Мои задачи:</p>
          <div class="tasks">
          <?php $array = array(
	'Разработка реляционной, инфологической и физической моделей',
	'Написание sql-запросов',
	'Подключение базы данных к сайту', 
	'Наполнение базы данных', 
	'Выдача заданий другим членам IT-команды проекта',);
    $array = array_chunk($array, ceil(count($array)));
    ?>
    <div class="city_list">
	<?php foreach($array as $items): ?>
	<ul>
		<?php foreach($items as $row): ?>
		<li><?php echo $row; ?></li>
		<?php endforeach; ?>
	</ul> 
	<?php endforeach; ?>
    </div>
    
        </div>
        </section>
    </main>
    <footer class="foot" id="foot">
      <div>
      <h2>Мои контакты:</h2>
      <p>Телефон: <a href="tel:+7 (996) 476-17-90">+7 (996) 476-17-90</a></p>
      <p>Email: <a href="mailto: liza.tolk@yandex.ru">liza.tolk@yandex.ru</a></p>
      <p>TG: <a href="https://t.me/babybooops">@babybooops</a></p>
      <p>Сформировано <?php date_default_timezone_set("Europe/Moscow"); echo date("d.m.Y"); ?> в <?php date_default_timezone_set("Europe/Moscow"); echo date('H-i:s') ?> </p>
      </div>
    </footer>
  </body>
</html>