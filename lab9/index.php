<html lang="ru">
  <head>
    <meta charset="utf-8">
    <title>Лабораторная 9</title>
    <link rel="stylesheet" href="style.css">
  </head>
  <body>
    <div class="wrapper">
    <header class="head" id="head">
        <table>
            <tr><th colspan="2" height="20"><div>
                <div>
                    <span></span>
                </div> </th>
            </tr> 
            <tr><td><div>
                <div>
                   <img src="logo.jpg" width="300"> 
                </div></td>
                <td>
                    <p class="page-title"><h1>Толкачева Елизавета Викторовна 211-361</h1></p>
                    <p><h1>Лабораторная работа №9 В-2</h1></p>
                </td></tr> 
            </table>
    </header>
    <main>
      <div class="content">
      <article>
        <section class="mainpage" id="mainpage">
        <?php
          $x = -10;	
          $encounting = 20;	
          $step = 1;	
          $type = 'D';	
          $max = -100000000000000;
          $min = 100000000000000;
          $mean = 0;
          $count = 0;
          $sum = 0;
          
          switch ($type) {
            case 'B':
                echo '<ul>';
                break;
            case 'C':
                echo '<ol>';
                break;
            case 'D':
                echo '<table class="tableD"><tr><th>Номер столбца</th><th>x</th><th>f(x)</th></tr>';
                break;
            case 'E':
               echo '<div>';
        }
          for( $i=0; $i < $encounting; $i++, $x += $step )
          {
            if($x == 0) {
              $f = 'error';	
          
            }	
           else
           { if( $x <= 10 )	
            {
              $f = (10 + $x) / $x;
            }
            	
            elseif( $x > 10 && $x < 20 )	
            {
              $f = $x / 7 * ($x - 2);	
            } else
            {	
              $f = $x * 8 + 2;	
            }
            $f = round($f, 3);
            if($max <= $f) {
              $max = $f;
            }	
            if($min >= $f) {
              $min = $f;
            }
            $sum += $f;
            $count += 1;
            $mean = $sum / $count;
          }
            switch ($type) {
              case 'A':
                  echo 'f('.$x.')='.$f.'<br>';
                  break;
              case 'B':
                  echo '<li>f('.$x.')='.$f.'</li>';
                  break;
              case 'C':
                  echo '<li>f('.$x.')='.$f.'</li>';
                  break;
              case 'D':
                  echo '<tr>
                  <td>'.($i + 1).'</td>
                  <td>'.($x).'</td>
                  <td>'.($f).'</td>
                </tr>';
                  break;
              case 'E':
                 echo '<div class="e">f('.$x.')='.$f.'</div>';
          }
        }
          switch ($type) {
            case 'B':
                echo '</ul>';
                break;
            case 'C':
                echo '</ol>';
                break;
            case 'D':
                echo '</table>';
                break;
            case 'E':
               echo '</div>';
        }
          echo 'max = '.round($max, 3).'<br>';
          echo 'min = '.round($min, 3).'<br>';
          echo 'sum = '.round($sum, 3).'<br>';
          echo 'arithmetic mean = '.round($mean, 3).'<br>'

          ?>
          </div>
    </main>
    <div class="footer">
    <footer class="foot" id="foot">
      <h2>Тип верстки </h2><?php echo $type ?>
        </div>
    </footer>
    </div>
  </body>
</html>
