<html lang="ru">
  <head>
    <meta charset="utf-8">
    <title> iPhone13
  </title>
    <link rel="stylesheet" href="style.css">
  </head>
  <body>
  <header class="head" id="head">
      <p class="page-title"><h1>Магазин</h1></p>
      <nav class="blog-navigation">
      <a href="index.php">Главная</a>
        <a href="#foot">Наши контакты</a>
        <a href="products.php">Продукция</a>
        <a href="login.php">Авторизация</a>
        <a href="reg.php">Регистрация</a>
      </nav>
    </header>
    <main class = "content">
  <section class="iphone13" id="iphone13">
  <img src="logo.png" width="300">
          <h2>iPhone 13</h2>
          <img src="iphone13_1.jpg" width="900">
          <p>Осталось в наличии: 50 штук</p>
          <p>iPhone 13 — актуальный базовый смартфон Apple</p>
<p>Процессор	Apple A15 Bionic</p>
<p>Оперативная память	4 Гб</p>
<p>Встроенная память	128, 256 или 512 Гб</p>
<p>Батарея	3095 мА·ч</p>
<p>Экран	6,1 дюйма, AMOLED, 60 гц</p>
<p>Основная камера	12 Мп с f/1,6 + 12 Мп с f/2,4 (сверхширокоугольная)</p>
<p>Фронтальная камера	12 Мп</p>
<p>Интересные особенности:	вырез для Face ID меньше на 20%, сенсоры камеры от iPhone 12 Pro Max, телефон работает на 1,5—2 часа дольше, чем iPhone 12: в районе 7—8 часов</p>
        </section>
        </div>
        </section>
    </main>
    <footer class="foot" id="foot">
      <div>
      <h2>Магазин электроники и бытовой техники</h2>
      <h2>Наши контакты:</h2>
      <p>Адрес: г. Москва, м. Автозаводская, ул. Пушкина, д. 1</a></p>
      <p>Телефон: <a href="tel:+7 (996) 476-17-90">+7 (996) 476-17-90</a></p>
      <p>Email: shop@yandex.ru</p>
      <p>TG: @shop</a></p>
      </div>
    </footer>
  </body>
</html>