<html lang="ru">
  <head>
    <meta charset="utf-8">
    <title> Магазин
  </title>
    <link rel="stylesheet" href="style.css">
  </head>
  <body>
  <header class="head" id="head">
      <p class="page-title"><h1>Магазин</h1></p>
      <nav class="blog-navigation">
        <a href="#mainpage">Главная</a>
        <a href="#shop">Ассортимент</a>
        <a href="#foot">Наши контакты</a>
        <a href="products.php">Продукция</a>
        <a href="login.php">Авторизация</a>
        <a href="reg.php">Регистрация</a>
      </nav>
    </header>
    <main class = "content">
  <section class="mainpage" id="mainpage">
  <img src="logo.png" width="300">
          <h2>Магазин электроники и бытовой техники</h2>
          <p>Уважаемые покупатели, в нашем магазине вы можете приобрести современную бытовую технику по низким ценам. </p>
          <p>Магазин находится в г. Москва, но возможна доставка в любой уголок России.</p>
        </section>
            <table class="table">
              <caption id="shop"><h2>Наш ассортимент</h2></caption> 
              <tr>
                <td width = 25%>
                  <div>
                    <h4>Телефоны</h4>
                    <p> <ul class="list">
            <li>Iphone</li>
            <li>Samsung</li>
            <li>Huawei</li>
            <li>Realme</li>
            <li>Xiaomi</li>
            <li>...</li>
          </ul></p>
                  </div>
                </td>
                <td width = 25%>
                  <div>
                    <h4>Компьютеры и ноутбуки</h4>
                    <p> <ul class="list">
            <li>MAC</li>
            <li>HP</li>
            <li>Huawei</li>
            <li>Asus</li>
            <li>Acer</li>
            <li>...</li>
          </ul></p>
                  </div>
                </td>
                <td width = 25%>
                  <div>
                    <h4>Бытовая техника</h4>
                  
            <li>Холодильники</li>
            <li>Стиральные машины</li>
            <li>Посудомоечные машины</li>
            <li>Обогреватели</li>
            <li>Пылесосы</li>
            <li>...</li>
                  </div>
                </td>
                <td width = 25%>
                  <div>
                    <h4>Электроника</h4>
                    <li>Телевизоры</li>
            <li>Наушники</li>
            <li>Колонки</li>
            <li>Компьтерные мыши</li>
            <li>...</li>
                  </div>
                </td>
              </tr>
          </table>
        </section>
        </div>
        </section>
    </main>
    <footer class="foot" id="foot">
      <div>
      <h2>Магазин электроники и бытовой техники</h2>
      <h2>Наши контакты:</h2>
      <p>Адрес: г. Москва, м. Автозаводская, ул. Пушкина, д. 1</a></p>
      <p>Телефон: <a href="tel:+7 (996) 476-17-90">+7 (996) 476-17-90</a></p>
      <p>Email: shop@yandex.ru</p>
      <p>TG: @shop</a></p>
      </div>
    </footer>
  </body>
</html>