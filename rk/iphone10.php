<html lang="ru">
  <head>
    <meta charset="utf-8">
    <title> iPhone10
  </title>
    <link rel="stylesheet" href="style.css">
  </head>
  <body>
  <header class="head" id="head">
      <p class="page-title"><h1>Магазин</h1></p>
      <nav class="blog-navigation">
      <a href="index.php">Главная</a>
        <a href="#foot">Наши контакты</a>
        <a href="products.php">Продукция</a>
        <a href="login.php">Авторизация</a>
        <a href="reg.php">Регистрация</a>
      </nav>
    </header>
    <main class = "content">
  <section class="iphone10" id="iphone10">
  <img src="logo.png" width="300">
          <h2>iPhone X</h2>
          <img src="iphone10_1.jpg" width="500">
          <p>Осталось в наличии: 15 штук</p>
          <p>Технические характеристики Apple iPhone X</p>
<p>SoC Apple A11 Bionic (6 ядер, 2 из которых высокопроизводительные и работают на частоте 2,1 ГГц, а 4 — энергоэффективные)</p>
<p>GPU Apple A11 Bionic</p>
<p>Сопроцессор движения Apple M11, включающий барометр, акселерометр, гироскоп и компас</p>
<p>RAM 3 ГБ</p>
<p>Флэш-память 64/256 ГБ</p>
<p>Поддержка карт памяти отсутствует</p>
<p>Операционная система iOS 11</p>
<p>Сенсорный дисплей OLED, 5,8″, 2436×1125 (458 ppi), емкостной, мультитач, поддержка технологий 3D Touch и отклика Taptic Engine</p>
<p>Распознавание лица с помощью камеры TrueDepth</p>
        </section>
        </div>
        </section>
    </main>
    <footer class="foot" id="foot">
      <div>
      <h2>Магазин электроники и бытовой техники</h2>
      <h2>Наши контакты:</h2>
      <p>Адрес: г. Москва, м. Автозаводская, ул. Пушкина, д. 1</a></p>
      <p>Телефон: <a href="tel:+7 (996) 476-17-90">+7 (996) 476-17-90</a></p>
      <p>Email: shop@yandex.ru</p>
      <p>TG: @shop</a></p>
      </div>
    </footer>
  </body>
</html>